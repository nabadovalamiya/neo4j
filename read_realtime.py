import boto3
from py2neo import Graph
import random


def find_third_post(liked_posts):
    # Retrieve all post IDs
    all_post_ids = [1, 2, 3]  # Replace with your actual post IDs or retrieve from Neo4j

    # Find the third post that the user hasn't liked
    unliked_posts = list(set(all_post_ids) - set(liked_posts))

    if len(unliked_posts) > 0:
        return random.choice(unliked_posts)
    else:
        return None

# Connect to Neo4j - replace credentials and host with your own
graph = Graph("bolt://ec2-3-83-254-136.compute-1.amazonaws.com:7687", auth=("neo4j", "A123456a"))

# Execute Cypher query to retrieve user data with their liked posts and friend count
query = """
MATCH (u:User)
OPTIONAL MATCH (u)-[:LIKES]->(p:Post)
OPTIONAL MATCH (u)-[:IS_FRIEND]-(f:User)
RETURN u, collect(p.id) AS liked_posts, count(distinct f) as friend_count
"""

result = graph.run(query)

# Create an S3 client - replace 'YOUR_ACCESS_KEY' and 'YOUR_SECRET_KEY' with your own
s3_client = boto3.client('s3', aws_access_key_id='AKIAUANQZ6XIKPB57DGL', aws_secret_access_key='lYCCTOUwJAykVewfq2JYwewVFEHt2eCFZbD8fsyI')

# Bucket name and file name in S3
bucket_name = 'neo4j-data-bucket'
file_name = 'alert_messages.txt'

# Retrieve the existing content of the file from the S3 bucket
existing_content = ""
try:
    existing_object = s3_client.get_object(Bucket=bucket_name, Key='DATA/alertmessage.txt')
    existing_content = existing_object['Body'].read().decode('utf-8')
except:
    pass

# Loop through the result and generate alert messages for users who like two posts and have more than 10 friends
alert_messages = []
for record in result:
    user = record['u']
    liked_posts = record['liked_posts']
    friend_count = record['friend_count']

    # Check if the user liked exactly two posts and has more than 10 friends
    if len(liked_posts) == 2 and friend_count > 10:
        third_post_id = find_third_post(liked_posts)

        if third_post_id is not None:
            # Generate alert message to send the user's friends an ad about the third post
            message = f"Alert: Send ad about Post {third_post_id} to friends of User who has more than 10 friends {user['id']}"
            alert_messages.append(message)

# Join the existing content, alert messages, and newlines
messages_str = existing_content + '\n' + '\n'.join(alert_messages)

# Upload the messages to the S3 bucket
s3_client.put_object(Body=messages_str, Bucket=bucket_name, Key='DATA/alertmessage.txt')
