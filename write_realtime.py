from py2neo import Graph, Node, Relationship
import random
import time
import os

graph = Graph("bolt://ec2-3-83-254-136.compute-1.amazonaws.com:7687", auth=("neo4j", "A123456a"))
unique_id = int(time.time())
users = []

for i in range(1, 21):
    email = f"user{i}@example.com"
    user_id = f"user_{unique_id}_{i}"
    user = Node("User", id=user_id, name=f"User {user_id}", email=email)
    user.__primarylabel__ = "User"
    user.__primarykey__ = "id"
    users.append(user)
    graph.merge(user)

posts = []
for i in range(1, 4):
    post = Node("Post", id=i, title=f"Post {i}")
    post.__primarylabel__='Post'
    post.__primarykey__='title'
    posts.append(post)
    graph.merge(post)

for user in users:
    num_friends = random.randint(1, 6)
    potential_friends = [u for u in users if u != user]
    for i in range(num_friends):
        friend = random.choice(potential_friends)
        rel = Relationship(user, "IS_FRIEND", friend)
        graph.merge(rel)
        rel_inverse = Relationship(friend, "IS_FRIEND", user)
        graph.merge(rel_inverse)
        potential_friends.remove(friend)

for user in users:
    num_likes = random.randint(1, 3)
    liked_posts = random.sample(posts, num_likes)
    for post in liked_posts:
        rel = Relationship(user, "LIKES", post)
        graph.merge(rel)

